

# PostgreSQL
```postgresql
create type enum_draft_area as enum ('default', 'head', 'foot');
alter type enum_draft_area owner to postgres;

create table draft
(
	draft_uuid uuid not null
		constraint draft_pk
			primary key,
	draft_lang_iso enum_supported_lang_iso not null,
	draft_unique varchar(200) not null,
	draft_text text not null,
	draft_area enum_draft_area default 'default'::enum_draft_area not null
);

comment on column draft.draft_unique is 'technical grouping e.g. best_regards_default';

alter table draft owner to postgres;

create unique index draft_draft_lang_iso_draft_unique_uindex
	on draft (draft_lang_iso, draft_unique);

create table draft_category
(
	draft_category_uuid uuid not null
		constraint draft_category_pk
			primary key,
	draft_category_label varchar(200) not null
);

alter table draft_category owner to postgres;

create table draft_category_relation
(
	draft_category_relation_uuid uuid not null
		constraint draft_category_relation_pk
			primary key,
	draft_uuid uuid not null
		constraint draft_category_relation_draft_draft_uuid_fk
			references draft,
	draft_category_uuid uuid not null
		constraint draft_category_relation_draft_category_draft_category_uuid_fk
			references draft_category
);

alter table draft_category_relation owner to postgres;

create unique index draft_category_relation_draft_uuid_draft_category_uuid_uindex
	on draft_category_relation (draft_uuid, draft_category_uuid);
```
