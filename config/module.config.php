<?php

namespace Bitkorn\Draft;

use Bitkorn\Draft\Controller\Ajax\DraftListsController;
use Bitkorn\Draft\Controller\Rest\DraftRestController;
use Bitkorn\Draft\Controller\Rest\DraftTextRestController;
use Bitkorn\Draft\Entity\ParamsDraftNew;
use Bitkorn\Draft\Factory\Controller\Ajax\DraftListsControllerFactory;
use Bitkorn\Draft\Factory\Controller\Rest\DraftRestControllerFactory;
use Bitkorn\Draft\Factory\Controller\Rest\DraftTextRestControllerFactory;
use Bitkorn\Draft\Factory\Entity\ParamsDraftNewFactory;
use Bitkorn\Draft\Factory\Form\DraftFormFactory;
use Bitkorn\Draft\Factory\Form\DraftTextFormFactory;
use Bitkorn\Draft\Factory\Service\DraftServiceFactory;
use Bitkorn\Draft\Factory\Table\DraftCategoryRelationTableFactory;
use Bitkorn\Draft\Factory\Table\DraftCategoryTableFactory;
use Bitkorn\Draft\Factory\Table\DraftTableFactory;
use Bitkorn\Draft\Factory\Table\DraftTextTableFactory;
use Bitkorn\Draft\Form\DraftForm;
use Bitkorn\Draft\Form\DraftTextForm;
use Bitkorn\Draft\Service\DraftService;
use Bitkorn\Draft\Table\DraftCategoryRelationTable;
use Bitkorn\Draft\Table\DraftCategoryTable;
use Bitkorn\Draft\Table\DraftTable;
use Bitkorn\Draft\Table\DraftTextTable;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router'          => [
        'routes' => [
            /*
             * REST
             */
            'bitkorn_draft_rest_draft'                          => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/draft-rest-draft[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => DraftRestController::class,
                    ],
                ],
            ],
            'bitkorn_draft_rest_drafttext'                      => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/draft-rest-draft-text[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => DraftTextRestController::class,
                    ],
                ],
            ],
            /*
             * AJAX - draftLists
             */
            'bitkorn_draft_ajax_draftlists_enumdraftareaassoc'  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/draft-area-assoc',
                    'defaults' => [
                        'controller' => DraftListsController::class,
                        'action'     => 'enumDraftAreaAssoc'
                    ],
                ],
            ],
            'bitkorn_draft_ajax_draftlists_draftcategoryaassoc' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/draft-category-assoc',
                    'defaults' => [
                        'controller' => DraftListsController::class,
                        'action'     => 'draftCategoryAssoc'
                    ],
                ],
            ],
            'bitkorn_draft_ajax_draftlists_draftcategorymap'    => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/draft-category-map',
                    'defaults' => [
                        'controller' => DraftListsController::class,
                        'action'     => 'draftCategoryMap'
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            DraftRestController::class     => DraftRestControllerFactory::class,
            DraftTextRestController::class => DraftTextRestControllerFactory::class,
            // AJAX
            DraftListsController::class    => DraftListsControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories'  => [
            // service
            DraftService::class               => DraftServiceFactory::class,
            // table
            DraftTable::class                 => DraftTableFactory::class,
            DraftTextTable::class             => DraftTextTableFactory::class,
            DraftCategoryTable::class         => DraftCategoryTableFactory::class,
            DraftCategoryRelationTable::class => DraftCategoryRelationTableFactory::class,
            // form
            DraftForm::class                  => DraftFormFactory::class,
            DraftTextForm::class              => DraftTextFormFactory::class,
            // entity
            ParamsDraftNew::class             => ParamsDraftNewFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'    => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'    => [
        'template_map'        => [],
        'template_path_stack' => [],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'bitkorn_draft'   => [
        /**
         * Map unique strings to uuids. Use the unique strings in the GUI.
         */
        'draft_category_map' => [
            'default'               => '130dcbd7-1c15-42d5-b28a-58d9a9fe359d',
            'order'                 => '2e73a5c0-4a9b-4cc4-9875-96f1b8ace640',
            'purchase_order'        => 'd49b9b17-1a60-44c5-a67c-9ceabc08b64e',
            'purchase_request'      => '1cd190ef-e23a-480b-8419-6d2c28774c65',
            'offer'                 => '76a15e13-7bb6-41b4-abaa-32b4c0e03a04',
            'invoice'               => '0b1f671a-1952-4357-b31d-048513bdca38',
            'delivery'              => '33606e60-ee13-422e-a7a2-698a0295f317',
            'order_confirm'         => '08fb6461-f405-4f48-8709-66bca2fd658c',
            'camo'                  => 'ff77bb6e-bb52-4c11-b530-9e71c77ef681',
            'invoice_delivery_summ' => '78770695-052d-476a-9a7a-3056b398fdd3',
        ],
    ],
];
