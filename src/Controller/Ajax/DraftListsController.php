<?php

namespace Bitkorn\Draft\Controller\Ajax;

use Bitkorn\Draft\Service\DraftService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;

class DraftListsController extends AbstractUserController
{
    protected DraftService $draftService;

    public function setDraftService(DraftService $draftService): void
    {
        $this->draftService = $draftService;
    }

	/**
	 * @return JsonModel
	 */
	public function enumDraftAreaAssocAction(): JsonModel
    {
		$jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->draftService->getDraftAreasAssoc());
        $jsonModel->setSuccess(1);
		return $jsonModel;
	}

    /**
     * @return JsonModel
     */
    public function draftCategoryAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->draftService->getDraftCategoryUuidAssoc());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    public function draftCategoryMapAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if(!empty($map = $this->draftService->getDraftCategoryMap())) {
            $jsonModel->setObj($map);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
