<?php

namespace Bitkorn\Draft\Controller\Rest;

use Bitkorn\Draft\Entity\DraftEntity;
use Bitkorn\Draft\Entity\ParamsDraftNew;
use Bitkorn\Draft\Form\DraftForm;
use Bitkorn\Draft\Service\DraftService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class DraftRestController extends AbstractUserRestController
{
    protected ParamsDraftNew $paramsDraftNew;
    protected DraftForm $draftForm;
    protected DraftService $draftService;

    public function setParamsDraftNew(ParamsDraftNew $paramsDraftNew): void
    {
        $this->paramsDraftNew = $paramsDraftNew;
    }

    public function setDraftForm(DraftForm $draftForm): void
    {
        $this->draftForm = $draftForm;
    }

    public function setDraftService(DraftService $draftService): void
    {
        $this->draftService = $draftService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->paramsDraftNew->setFromParamsArray($data);
        if (!$this->paramsDraftNew->isValid()) {
            $jsonModel->addMessages($this->paramsDraftNew->getMessages());
            return $jsonModel;
        }
        if (!empty($draftUnique = $this->draftService->insertDraftWithParamsDarftNew($this->paramsDraftNew))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setVal($draftUnique);
        }
        return $jsonModel;
    }

    /**
     * Update draft_area, draft_label, draft_categories & draft_area.
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['draft_uuid'] = $id;
        $this->draftForm->setPrimaryKeyAvailable(true);
        $this->draftForm->setData($data);
        if (!$this->draftForm->isValid()) {
            $jsonModel->addMessages($this->draftForm->getMessages());
            return $jsonModel;
        }
        $draftEntity = new DraftEntity();
        if (!$draftEntity->exchangeArrayFromDatabase($this->draftForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $draftCatUuids = filter_var_array($data, ['draft_category_uuids_arr' => ['filter' => FILTER_SANITIZE_EMAIL, 'flags' => FILTER_REQUIRE_ARRAY]]);
        if (!isset($draftCatUuids['draft_category_uuids_arr']) || !is_array($draftCatUuids['draft_category_uuids_arr'])) {
            $jsonModel->addMessage('It must be at least one draft category provided');
            return $jsonModel;
        }
        $uuid = new Uuid();
        foreach ($draftCatUuids['draft_category_uuids_arr'] as $draftCatUuid) {
            if (!$uuid->isValid($draftCatUuid)) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
                return $jsonModel;
            }
        }
        $draftEntity->setDraftCategoryUuidsArr($draftCatUuids['draft_category_uuids_arr']);
        if ($this->draftService->updateDraft($draftEntity)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->draftService->deleteDraftComplete($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

}
