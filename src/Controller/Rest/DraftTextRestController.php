<?php

namespace Bitkorn\Draft\Controller\Rest;

use Bitkorn\Draft\Entity\DraftTextEntity;
use Bitkorn\Draft\Entity\ParamsDraft;
use Bitkorn\Draft\Form\DraftTextForm;
use Bitkorn\Draft\Service\DraftService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;

class DraftTextRestController extends AbstractUserRestController
{
    protected DraftTextForm $draftTextForm;
    protected DraftService $draftService;

    public function setDraftTextForm(DraftTextForm $draftTextForm): void
    {
        $this->draftTextForm = $draftTextForm;
    }

    public function setDraftService(DraftService $draftService): void
    {
        $this->draftService = $draftService;
    }

    /**
     * Update the text of a db.draft_text (draft_text_text).
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['draft_text_uuid'] = $id;
        $this->draftTextForm->setPrimaryKeyAvailable(true);
        $this->draftTextForm->setData($data);
        if (!$this->draftTextForm->isValid()) {
            $jsonModel->addMessages($this->draftTextForm->getMessages());
            return $jsonModel;
        }
        $draftTextEntity = new DraftTextEntity();
        if (!$draftTextEntity->exchangeArrayFromDatabase($this->draftTextForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->draftService->updateDraftText($draftTextEntity)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $paramsDraft = new ParamsDraft();
        $paramsDraft->setFromParamsArray($this->params()->fromQuery());
        $jsonModel->setArr($this->draftService->searchDraftTexts($paramsDraft));
        $jsonModel->setCount($this->draftService->getDraftCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
