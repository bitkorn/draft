<?php

namespace Bitkorn\Draft\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class DraftEntity extends AbstractEntity
{
    public array $mapping = [
        'draft_uuid'               => 'draft_uuid',
        'draft_area'               => 'draft_area',
        'draft_label'              => 'draft_label',
        // join
        'draft_category_uuid'      => 'draft_category_uuid', // at creation time, there must be a category
        'draft_category_label'     => 'draft_category_label',
        // multiple draft_category_uuids for db.draft_category_relation
        'draft_category_uuids_arr' => 'draft_category_uuids_arr',
    ];

    protected $primaryKey = 'draft_uuid';

    public function getDraftUuid(): string
    {
        if (!isset($this->storage['draft_uuid'])) {
            return '';
        }
        return $this->storage['draft_uuid'];
    }

    public function setDraftUuid(string $draftUuid): void
    {
        $this->storage['draft_uuid'] = $draftUuid;
    }

    public function getDraftArea(): string
    {
        if (!isset($this->storage['draft_area'])) {
            return '';
        }
        return $this->storage['draft_area'];
    }

    public function setDraftArea(string $draftArea): void
    {
        $this->storage['draft_area'] = $draftArea;
    }

    public function getDraftLabel(): string
    {
        if (!isset($this->storage['draft_label'])) {
            return '';
        }
        return $this->storage['draft_label'];
    }

    public function setDraftLabel(string $draftLabel): void
    {
        $this->storage['draft_label'] = $draftLabel;
    }

    public function getDraftCategoryUuid(): string
    {
        if (!isset($this->storage['draft_category_uuid'])) {
            return '';
        }
        return $this->storage['draft_category_uuid'];
    }

    public function setDraftCategoryUuid(string $draftCategoryUuid): void
    {
        $this->storage['draft_category_uuid'] = $draftCategoryUuid;
    }

    public function getDraftCategoryLabel(): string
    {
        if (!isset($this->storage['draft_category_label'])) {
            return '';
        }
        return $this->storage['draft_category_label'];
    }

    public function setDraftCategoryLabel(string $draftCategoryLabel): void
    {
        $this->storage['draft_category_label'] = $draftCategoryLabel;
    }

    public function getDraftCategoryUuidsArr(): array
    {
        if (!isset($this->storage['draft_category_uuids_arr'])) {
            return [];
        }
        return $this->storage['draft_category_uuids_arr'];
    }

    public function setDraftCategoryUuidsArr(array $draftCategoryUuidsArr): void
    {
        $this->storage['draft_category_uuids_arr'] = $draftCategoryUuidsArr;
    }

}
