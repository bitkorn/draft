<?php

namespace Bitkorn\Draft\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class DraftTextEntity extends AbstractEntity
{
    public array $mapping = [
        'draft_text_uuid'     => 'draft_text_uuid',
        'draft_uuid'          => 'draft_uuid',
        'draft_text_lang_iso' => 'draft_text_lang_iso',
        'draft_text_text'     => 'draft_text_text',
    ];

    protected $primaryKey = 'draft_text_uuid';

    public function getDraftTextUuid(): string
    {
        if (!isset($this->storage['draft_text_uuid'])) {
            return '';
        }
        return $this->storage['draft_text_uuid'];
    }

    public function setDraftTextUuid(string $draftTextUuid): void
    {
        $this->storage['draft_text_uuid'] = $draftTextUuid;
    }

    public function getDraftUuid(): string
    {
        if (!isset($this->storage['draft_uuid'])) {
            return '';
        }
        return $this->storage['draft_uuid'];
    }

    public function setDraftUuid(string $draftUuid): void
    {
        $this->storage['draft_uuid'] = $draftUuid;
    }

    public function getDraftTextLangIso(): string
    {
        if (!isset($this->storage['draft_text_lang_iso'])) {
            return '';
        }
        return $this->storage['draft_text_lang_iso'];
    }

    public function setDraftTextLangIso(string $draftTextLangIso): void
    {
        $this->storage['draft_text_lang_iso'] = $draftTextLangIso;
    }

    public function getDraftTextText(): string
    {
        if (!isset($this->storage['draft_text_text'])) {
            return '';
        }
        return $this->storage['draft_text_text'];
    }

    public function setDraftTextText(string $draftTextText): void
    {
        $this->storage['draft_text_text'] = $draftTextText;
    }
}
