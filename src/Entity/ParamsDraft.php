<?php

namespace Bitkorn\Draft\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;

class ParamsDraft extends ParamsBase
{
    protected array $orderFieldsAvailable = ['draft_text_lang_iso', 'draft_text_text', 'draft_area'];
    protected string $draftLangIso = '';
    protected string $draftText = '';
    protected string $draftArea = '';
    protected array $draftCategories = [];

    public function setDraftLangIso(string $draftLangIso): void
    {
        $this->draftLangIso = filter_var($draftLangIso, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public function setDraftText(string $draftText): void
    {
        $this->draftText = htmlspecialchars(filter_var($draftText, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]));
    }

    public function setDraftArea(string $draftArea): void
    {
        $this->draftArea = $draftArea;
    }

    public function setDraftCategories(array $draftCategories): void
    {
        $this->draftCategories = $draftCategories;
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setDraftLangIso($qp['draft_text_lang_iso'] ?: '');
        $this->setDraftText($qp['draft_text_text'] ?? '');
        $this->setDraftArea($qp['draft_area'] ?: '');
        $this->setDraftCategories(!empty($qp['draft_category_uuids_arr']) ? explode(',', $qp['draft_category_uuids_arr']) : []);
    }

    /**
     * Compute Select only for db.view_draft_text. Because it uses the table draft, draft_text and has the CSV field 'draft_category_uuids'.
     * @param Select $select
     * @param string $orderDefault
     */
    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        if (!$this->doCount) {
            parent::computeSelect($select);
        } else {
            $select->columns(['count_draft' => new Expression('COUNT(*)')]);
        }
        if (!empty($this->draftLangIso)) {
            $select->where->equalTo('draft_text_lang_iso', $this->draftLangIso);
        }
        if (!empty($this->draftText)) {
            $text = strtolower($this->draftText);
            $select->where->like(new Expression('LOWER(draft_text_text)'), '%' . $text . '%');
        }
        if (!empty($this->draftArea) && $this->draftArea != 'all') {
            $select->where->nest()->equalTo('draft_area', $this->draftArea)->or->equalTo('draft_area', 'all');
        }
        if (!empty($this->draftCategories)) {
            $predicate = null;
            foreach ($this->draftCategories as $draftCategory) {
                if (empty($draftCategory)) {
                    continue;
                }
                if (!isset($predicate)) {
                    $predicate = $select->where->NEST->like('draft_category_uuids', "%$draftCategory%");
                    continue;
                }
                $predicate->OR->like('draft_category_uuids', "%$draftCategory%");
            }
        }
    }
}
