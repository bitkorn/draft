<?php

namespace Bitkorn\Draft\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;

/**
 * This class is only for creation time.
 * You must it create from the service manager ...with his factory class (ParamsDraftNewFactory).
 */
class ParamsDraftNew extends ParamsBase
{
    protected bool $valid = true;
    protected array $draftAreasAvailable = [];
    protected array $langIsosAvailable = [];
    protected array $draftCategoryUuidsAvailable = [];
    protected string $draftArea = '';
    protected string $draftLabel = '';
    protected array $draftCategoryUuids = [];
    protected array $draftTexts = [];

    public function setDraftAreasAvailable(array $draftAreasAvailable): void
    {
        $this->draftAreasAvailable = $draftAreasAvailable;
    }

    public function setLangIsosAvailable(array $langIsosAvailable): void
    {
        $this->langIsosAvailable = $langIsosAvailable;
    }

    public function setDraftCategoryUuidsAvailable(array $draftCategoryUuidsAvailable): void
    {
        $this->draftCategoryUuidsAvailable = $draftCategoryUuidsAvailable;
    }

    public function setDraftArea(string $draftArea): void
    {
        $this->draftArea = filter_var($draftArea, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public function setDraftLabel(string $draftLabel): void
    {
        $this->draftLabel = filter_var($draftLabel, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public function setDraftCategoryUuids(array $draftCategoryUuids): void
    {
        $this->draftCategoryUuids = $draftCategoryUuids;
    }

    public function setDraftTexts(array $draftTexts): void
    {
        $this->draftTexts = $draftTexts;
    }

    /**
     * @var array Assoc array [langIso => text, langIso => text, ...]
     */
    public function getDraftTexts(): array
    {
        return $this->draftTexts;
    }

    /**
     * It does not call `parent::setFromParamsArray($qp);`.
     * @param array $qp
     */
    public function setFromParamsArray(array $qp): void
    {
        $this->setDraftArea($qp['draft_area'] ?? '');
        $this->setDraftLabel($qp['draft_label'] ?? '');

        // draft - categories
        $draftCatUuids = filter_var_array($qp, ['draft_category_uuids_arr' => ['filter' => FILTER_UNSAFE_RAW, 'flags' => FILTER_REQUIRE_ARRAY | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH]]);
        if (!isset($draftCatUuids['draft_category_uuids_arr']) || !is_array($draftCatUuids['draft_category_uuids_arr'])) {
            $this->valid = false;
            return;
        }
        foreach ($draftCatUuids['draft_category_uuids_arr'] as $draftCatUuid) {
            if (!in_array($draftCatUuid, $this->draftCategoryUuidsAvailable)) {
                $this->valid = false;
                return;
            }
        }
        $this->setDraftCategoryUuids($draftCatUuids['draft_category_uuids_arr']);

        // draft - texts
        $draftTextsArr = filter_var_array($qp, ['draft_texts_obj' => ['filter' => FILTER_UNSAFE_RAW, 'flags' => FILTER_REQUIRE_ARRAY | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH]]);
        if (!isset($draftTextsArr['draft_texts_obj']) || !is_array($draftTextsArr['draft_texts_obj'])) {
            $this->valid = false;
            return;
        }
        foreach ($draftTextsArr['draft_texts_obj'] as $lang => $text) {
            if (!in_array($lang, $this->langIsosAvailable)) {
                $this->valid = false;
                return;
            }
        }
        $this->setDraftTexts($draftTextsArr['draft_texts_obj']);
    }

    public function isValid(): bool
    {
        if (!$this->valid || empty($this->draftArea) || empty($this->draftCategoryUuids) || empty($this->draftTexts)
            || !is_array($this->draftCategoryUuids) || !is_array($this->draftTexts)) {
            return false;
        }
        return true;
    }

    public function getAsDraftEntity(): DraftEntity
    {
        $entity = new DraftEntity();
        $entity->setDraftArea($this->draftArea);
        $entity->setDraftLabel($this->draftLabel);
        $entity->setDraftCategoryUuidsArr($this->draftCategoryUuids);
        return $entity;
    }

    /**
     * @param string $draftUuid
     * @return DraftTextEntity[]
     */
    public function getAsDraftTextEntities(string $draftUuid): array
    {
        $entities = [];
        foreach ($this->draftTexts as $langIso => $draftText) {
            $entity = new DraftTextEntity();
            $entity->setDraftUuid($draftUuid);
            $entity->setDraftTextLangIso($langIso);
            $entity->setDraftTextText($draftText);
            $entities[] = $entity;
        }
        return $entities;
    }
}
