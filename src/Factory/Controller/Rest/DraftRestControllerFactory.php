<?php

namespace Bitkorn\Draft\Factory\Controller\Rest;

use Bitkorn\Draft\Controller\Rest\DraftRestController;
use Bitkorn\Draft\Entity\ParamsDraftNew;
use Bitkorn\Draft\Form\DraftForm;
use Bitkorn\Draft\Service\DraftService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class DraftRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new DraftRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setParamsDraftNew($container->get(ParamsDraftNew::class));
        $controller->setDraftForm($container->get(DraftForm::class));
        $controller->setDraftService($container->get(DraftService::class));
        return $controller;
    }
}
