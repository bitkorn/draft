<?php

namespace Bitkorn\Draft\Factory\Controller\Rest;

use Bitkorn\Draft\Controller\Rest\DraftTextRestController;
use Bitkorn\Draft\Form\DraftTextForm;
use Bitkorn\Draft\Service\DraftService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class DraftTextRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new DraftTextRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setDraftTextForm($container->get(DraftTextForm::class));
        $controller->setDraftService($container->get(DraftService::class));
        return $controller;
    }
}
