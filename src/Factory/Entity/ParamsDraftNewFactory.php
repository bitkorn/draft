<?php

namespace Bitkorn\Draft\Factory\Entity;

use Bitkorn\Draft\Entity\ParamsDraftNew;
use Bitkorn\Draft\Table\DraftCategoryTable;
use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ParamsDraftNewFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     */
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null)
    {
        $params = new ParamsDraftNew();
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $params->setDraftAreasAvailable($toolsTable->getEnumValuesPostgreSQL('enum_draft_area'));
        $params->setLangIsosAvailable($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        /** @var DraftCategoryTable $draftCategoryTable */
        $draftCategoryTable = $container->get(DraftCategoryTable::class);
        $params->setDraftCategoryUuidsAvailable($draftCategoryTable->getDraftCategoryUuids());
        return $params;
    }
}
