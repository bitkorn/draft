<?php

namespace Bitkorn\Draft\Factory\Service;

use Bitkorn\Draft\Service\DraftService;
use Bitkorn\Draft\Table\DraftCategoryRelationTable;
use Bitkorn\Draft\Table\DraftCategoryTable;
use Bitkorn\Draft\Table\DraftTable;
use Bitkorn\Draft\Table\DraftTextTable;
use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class DraftServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new DraftService();
        $service->setLogger($container->get('logger'));
        $service->setDraftConfig($container->get('config')['bitkorn_draft']);
        $service->setDraftTable($container->get(DraftTable::class));
        $service->setDraftTextTable($container->get(DraftTextTable::class));
        $service->setDraftCategoryTable($container->get(DraftCategoryTable::class));
        $service->setDraftCategoryRelationTable($container->get(DraftCategoryRelationTable::class));
        $service->setToolsTable($container->get(ToolsTable::class));
        return $service;
    }
}
