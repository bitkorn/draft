<?php

namespace Bitkorn\Draft\Form;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class DraftForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $draftAreas;

    public function setDraftAreas(array $draftAreas): void
    {
        $this->draftAreas = $draftAreas;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'draft_uuid']);
        }
        $this->add(['name' => 'draft_area']);
        $this->add(['name' => 'draft_label']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['draft_uuid'] = [
                'required'   => true,
                'filters'    => [['name' => SanitizeStringFilter::class],],
                'validators' => [['name' => Uuid::class,]]
            ];
        }

        $filter['draft_area'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => [
                        'haystack' => $this->draftAreas,
                    ]
                ]
            ]
        ];

        $filter['draft_label'] = [
            'required'      => false,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 180,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
