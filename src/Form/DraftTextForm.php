<?php

namespace Bitkorn\Draft\Form;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class DraftTextForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $langIsos;

    public function setLangIsos(array $langIsos): void
    {
        $this->langIsos = $langIsos;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'draft_text_uuid']);
        }
        if (!$this->primaryKeyAvailable) {
            $this->add(['name' => 'draft_text_lang_iso']);
        }
        $this->add(['name' => 'draft_text_text']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['draft_text_uuid'] = [
                'required'   => true,
                'filters'    => [['name' => SanitizeStringFilter::class],],
                'validators' => [['name' => Uuid::class,]]
            ];
        }

        if (!$this->primaryKeyAvailable) {
            // only new draft texts need th language
            $filter['draft_text_lang_iso'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    [
                        'name'    => InArray::class,
                        'options' => [
                            'haystack' => $this->langIsos,
                        ]
                    ]
                ]
            ];
        }

        $filter['draft_text_text'] = [
            'required'      => true,
            'filters'       => [
                ['name' => FilterChainStringSanitize::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 2,
                        'max'      => 60000,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
