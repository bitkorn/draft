<?php

namespace Bitkorn\Draft\Service;

use Bitkorn\Draft\Entity\DraftEntity;
use Bitkorn\Draft\Entity\DraftTextEntity;
use Bitkorn\Draft\Entity\ParamsDraft;
use Bitkorn\Draft\Entity\ParamsDraftNew;
use Bitkorn\Draft\Table\DraftCategoryRelationTable;
use Bitkorn\Draft\Table\DraftCategoryTable;
use Bitkorn\Draft\Table\DraftTable;
use Bitkorn\Draft\Table\DraftTextTable;
use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Table\ToolsTable;

class DraftService extends AbstractService
{
    protected array $draftConfig;
    protected DraftTable $draftTable;
    protected DraftTextTable $draftTextTable;
    protected DraftCategoryTable $draftCategoryTable;
    protected DraftCategoryRelationTable $draftCategoryRelationTable;
    protected ToolsTable $toolsTable;
    protected int $draftCount = 0;

    public function setDraftConfig(array $draftConfig): void
    {
        $this->draftConfig = $draftConfig;
    }

    public function setDraftTable(DraftTable $draftTable): void
    {
        $this->draftTable = $draftTable;
    }

    public function setDraftTextTable(DraftTextTable $draftTextTable): void
    {
        $this->draftTextTable = $draftTextTable;
    }

    public function setDraftCategoryTable(DraftCategoryTable $draftCategoryTable): void
    {
        $this->draftCategoryTable = $draftCategoryTable;
    }

    public function setDraftCategoryRelationTable(DraftCategoryRelationTable $draftCategoryRelationTable): void
    {
        $this->draftCategoryRelationTable = $draftCategoryRelationTable;
    }

    public function setToolsTable(ToolsTable $toolsTable): void
    {
        $this->toolsTable = $toolsTable;
    }

    public function getDraftCount(): int
    {
        return $this->draftCount;
    }

    /**
     * @param DraftEntity $draftEntity
     * @return string The new created draft_uuid
     */
    public function insertDraft(DraftEntity $draftEntity, bool $withTransaction = true): string
    {
        if ($withTransaction) {
            $connection = $this->beginTransaction($this->draftTable);
        }
        if (empty($draftUuid = $this->draftTable->insertDraftWithEntity($draftEntity))) {
            if ($withTransaction) {
                $connection->rollback();
            }
            return '';
        }
        if (!empty($draftCategoryUuid = $draftEntity->getDraftCategoryUuid())) {
            if (empty($relUuid = $this->draftCategoryRelationTable->insertDraftCategoryRelation($draftUuid, $draftCategoryUuid))) {
                if ($withTransaction) {
                    $connection->rollback();
                }
                return '';
            }
        }
        if (!empty($draftCategoryUuids = $draftEntity->getDraftCategoryUuidsArr()) && is_array($draftCategoryUuids)) {
            foreach ($draftCategoryUuids as $draftCategoryUuid) {
                if (empty($relUuid = $this->draftCategoryRelationTable->insertDraftCategoryRelation($draftUuid, $draftCategoryUuid))) {
                    if ($withTransaction) {
                        $connection->rollback();
                    }
                    return '';
                }
            }
        }
        if ($withTransaction) {
            $connection->commit();
        }
        return $draftUuid;
    }

    /**
     * @param DraftTextEntity $draftTextEntity
     * @return string draft_text_uuid
     */
    public function insertDraftText(DraftTextEntity $draftTextEntity): string
    {
        return $this->draftTextTable->insertDraftText($draftTextEntity);
    }

    /**
     * Insert a complete draft.
     * A complete draft contain
     * - the draft area (head, foot, all)
     * - the draft label
     * - some draft category uuids
     * - texts for ALL available languages (langIsosAvailable)
     * @param ParamsDraftNew $paramsDraftNew
     * @return string draft_unique
     */
    public function insertDraftWithParamsDarftNew(ParamsDraftNew $paramsDraftNew): string
    {
        $connection = $this->beginTransaction($this->draftTable);
        if (empty($draftUuid = $this->insertDraft($paramsDraftNew->getAsDraftEntity(), false))) {
            $connection->rollback();
            return '';
        }
        if (empty($textEntities = $paramsDraftNew->getAsDraftTextEntities($draftUuid))) {
            $connection->rollback();
            return '';
        }
        foreach ($textEntities as $textEntity) {
            if (empty($textUuid = $this->insertDraftText($textEntity))) {
                $connection->rollback();
                return '';
            }
        }
        $connection->commit();
        return $draftUuid;
    }

    /**
     * @param DraftTextEntity $draftTextEntity
     * @return bool
     */
    public function updateDraftText(DraftTextEntity $draftTextEntity): bool
    {
        return $this->draftTextTable->updateDraftTextText($draftTextEntity) >= 0;
    }

    public function updateDraft(DraftEntity $draftEntity): bool
    {
        return $this->draftCategoryRelationTable->deleteForDraft($draftEntity->getDraftUuid()) >= 0
            && $this->draftCategoryRelationTable->insertDraftCategoryRelations($draftEntity->getDraftUuid(), $draftEntity->getDraftCategoryUuidsArr())
            && $this->draftTable->updateDraft($draftEntity) >= 0;
    }

    public function deleteDraftComplete(string $draftUuid): bool
    {
        if (!$this->draftTable->existDraft($draftUuid)) {
            return false;
        }
        $connection = $this->beginTransaction($this->draftTable);
        if (
            $this->draftTextTable->deleteForDraft($draftUuid) < 1
            || $this->draftCategoryRelationTable->deleteForDraft($draftUuid) < 1
            || $this->draftTable->deleteDraft($draftUuid) < 1
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    /**
     * @param ParamsDraft $paramsDraft
     * @return array From db.view_draft ORDER BY draft_unique, draft_lang_iso
     */
    public function searchDraftTexts(ParamsDraft $paramsDraft): array
    {
        $paramsDraft->setDoCount(true);
        $this->draftCount = $this->draftTextTable->searchDraftTexts($paramsDraft)[0];
        $paramsDraft->setDoCount(false);
        $drafts = $this->draftTextTable->searchDraftTexts($paramsDraft);
        foreach($drafts as &$draft) {
            $draft['draft_category_uuids_arr'] = explode(',', $draft['draft_category_uuids']);
        }
        return $drafts;
    }

    public function searchDraftsByCategoryAndArea(ParamsBase $params, string $draftCategoryUuid, string $draftArea, bool $count = false): array
    {
        return $this->draftTable->searchDraftsByCategoryAndArea($params, $draftCategoryUuid, $draftArea, $count);
    }

    public function getDraftCategoryUuidAssoc(): array
    {
        return $this->draftCategoryTable->getDraftCategoryUuidAssoc();
    }

    public function getDraftAreasAssoc(): array
    {
        $e = $this->toolsTable->getEnumValuesPostgreSQL('enum_draft_area');
        $assoc = [];
        foreach ($e as $area) {
            $assoc[$area] = $area;
        }
        return $assoc;
    }

    public function getDraftCategoryMap(): array
    {
        if (!empty($map = $this->draftConfig['draft_category_map'])) {
            return $map;
        }
        return [];
    }
}
