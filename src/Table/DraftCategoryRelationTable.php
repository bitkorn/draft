<?php

namespace Bitkorn\Draft\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DraftCategoryRelationTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'draft_category_relation';

    /**
     * @param string $draftCategoryUuid
     * @return array
     */
    public function getDraftsByCategory(string $draftCategoryUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('draft_category', 'draft_category.draft_category_uuid = draft_category_relation.draft_category_uuid'
                , ['draft_category_label'], Select::JOIN_LEFT);
            $select->join('draft', 'draft.draft_uuid = draft_category_relation.draft_uuid'
                , ['draft_lang_iso', 'draft_unique', 'draft_text'], Select::JOIN_LEFT);
            $select->where(['draft_category_uuid' => $draftCategoryUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDraftCategoryRelation(string $draftUuid, string $draftCategoryUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['draft_uuid' => $draftUuid, 'draft_category_uuid' => $draftCategoryUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * Insert a new draft - ONLY if it not exists.
     * @param string $draftUuid
     * @param string $draftCategoryUuid
     * @return string
     */
    public function insertDraftCategoryRelation(string $draftUuid, string $draftCategoryUuid): string
    {
        if ($this->existDraftCategoryRelation($draftUuid, $draftCategoryUuid)) {
            $this->logger->warn(__CLASS__ . '()->' . __FUNCTION__
                . '() DraftCategoryRelation already exist; draftUuid: ' . $draftUuid . '; draftCategoryUuid: ' . $draftCategoryUuid);
            return '';
        }
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'draft_category_relation_uuid' => $uuid,
                'draft_uuid' => $draftUuid,
                'draft_category_uuid' => $draftCategoryUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteForDraft(string $draftUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['draft_uuid' => $draftUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function insertDraftCategoryRelations(string $draftUuid, array $draftCategoryUuids): bool
    {
        foreach($draftCategoryUuids as $draftCategoryUuid) {
            if(empty($this->insertDraftCategoryRelation($draftUuid, $draftCategoryUuid))) {
                return false;
            }
        }
        return true;
    }
}
