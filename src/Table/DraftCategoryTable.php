<?php

namespace Bitkorn\Draft\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class DraftCategoryTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'draft_category';

    /**
     * @param string $draftCategoryUuid
     * @return array
     */
    public function getDraftCategory(string $draftCategoryUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['draft_category_uuid' => $draftCategoryUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getDraftCategoryUuidAssoc(): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                foreach ($result->toArray() as $c) {
                    $assoc[$c['draft_category_uuid']] = $c['draft_category_label'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }

    public function getDraftCategoryUuids(): array
    {
        $select = $this->sql->select();
        $uuids = [];
        try {
            $select->columns(['draft_category_uuid']);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                foreach ($result->toArray() as $c) {
                    $uuids[] = $c['draft_category_uuid'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $uuids;
    }
}
