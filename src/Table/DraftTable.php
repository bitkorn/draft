<?php

namespace Bitkorn\Draft\Table;

use Bitkorn\Draft\Entity\DraftEntity;
use Bitkorn\Draft\Entity\ParamsDraft;
use Bitkorn\Trinket\Entity\ParamsBase;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class DraftTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'draft';

    /**
     * @param string $draftUuid
     * @return array From db.view_draft
     */
    public function getDraft(string $draftUuid): array
    {
        $select = new Select('view_draft');
        try {
            $select->where(['draft_uuid' => $draftUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existDraft(string $draftUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->columns(['count_draft' => new Expression('COUNT(*)')]);
            $select->where(['draft_uuid' => $draftUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0]['count_draft'] > 0;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    /**
     * @param ParamsBase $params
     * @param string $draftCategoryUuid
     * @param string $draftArea
     * @param bool $count
     * @return array List of drafts or an array with count-drafts (['count_drafts' => 42]).
     */
    public function searchDraftsByCategoryAndArea(ParamsBase $params, string $draftCategoryUuid, string $draftArea, bool $count): array
    {
        $select = $this->sql->select();
        try {
            if ($count) {
                $select->columns(['count_drafts' => new Expression('COUNT(*)')]);
            }
            if ($draftCategoryUuid) {
                $selectRel = new Select('draft_category_relation');
                $selectRel->columns(['draft_uuid']);
                $selectRel->where(['draft_category_uuid' => $draftCategoryUuid]);

                $select->where->in('draft_uuid', $selectRel);
            }
            if ($draftArea) {
                $select->where(['draft_area' => $draftArea]);
            }
            if (!$count) {
                $params->computeSelect($select);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            $q = $select->getSqlString($this->getAdapter()->getPlatform());
            if ($result->valid() && $result->count() > 0) {
                if ($count) {
                    return $result->toArray()[0];
                } else {
                    return $result->toArray();
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDraftWithEntity(DraftEntity $draftEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'draft_uuid'  => $uuid,
                'draft_area'  => $draftEntity->getDraftArea(),
                'draft_label' => $draftEntity->getDraftLabel(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDraft(DraftEntity $draftEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'draft_area'  => $draftEntity->getDraftArea(),
                'draft_label' => $draftEntity->getDraftLabel(),
            ]);
            $update->where(['draft_uuid' => $draftEntity->getUuid()]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteDraft(string $draftUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['draft_uuid' => $draftUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
