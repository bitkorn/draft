<?php

namespace Bitkorn\Draft\Table;

use Bitkorn\Draft\Entity\DraftTextEntity;
use Bitkorn\Draft\Entity\ParamsDraft;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class DraftTextTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'draft_text';

    /**
     * @param string $draftTextUuid
     * @return array
     */
    public function getDraftText(string $draftTextUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['draft_text_uuid' => $draftTextUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertDraftText(DraftTextEntity $draftTextEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'draft_text_uuid'     => $uuid,
                'draft_uuid'          => $draftTextEntity->getDraftUuid(),
                'draft_text_lang_iso' => $draftTextEntity->getDraftTextLangIso(),
                'draft_text_text'     => $draftTextEntity->getDraftTextText(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateDraftTextText(DraftTextEntity $draftTextEntity): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['draft_text_text' => $draftTextEntity->getDraftTextText()])
                ->where(['draft_text_uuid' => $draftTextEntity->getDraftTextUuid()]);
            $q = $update->getSqlString($this->getAdapter()->getPlatform());
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param ParamsDraft $paramsDraft
     * @return array From db.view_draft ORDER BY draft_unique, draft_lang_iso
     */
    public function searchDraftTexts(ParamsDraft $paramsDraft): array
    {
        $select = new Select('view_draft_text');
        try {
            $paramsDraft->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if ($paramsDraft->isDoCount()) {
                    return [$result->toArray()[0]['count_draft']];
                }
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteForDraft(string $draftUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['draft_uuid' => $draftUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
